# Commune

Commune is a very simple web application for creating and running online elections and referendums. It was originally built for the [Republican Socialist Platform](https://republicansocialists.scot/) in Scotland, but is now freely available for anyone to use.

Commune supports [first-past-the-post](https://en.wikipedia.org/wiki/First-past-the-post_voting) elections/referendums as well as those conducted using the [single transferable vote](https://en.wikipedia.org/wiki/Single_transferable_vote). Commune's implementation of STV is based on that used for local elections in Scotland, as set out in Schedule 1, Part III of [The Scottish Local Government Elections Order 2007](https://www.legislation.gov.uk/ssi/2007/42/schedule/1/part/III/crossheading/counting-of-votes/made). It does, currently, differ in some relatively minor respects, which could lead to different outcomes in some fringe cases.

## Server requirements

Commune is built using the Laravel 8.x PHP framework and shares similar requirements, namely:

- PHP >= 7.3.0
- MySQL >= 5.7.0
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Installation

You can download the latest version of Commune by running:

```
git clone https://gitlab.com/cdbeaton/commune
```

We recommend having a dedicated domain or subdomain for your Commune installation. You should point your domain at Commune's "public" subdirectory rather than the root directory.

Navigate to the Commune root directory and install the dependencies using [Composer](https://getcomposer.org/doc/00-intro.md):

```
composer install
```

You now need to configure Commune. Make a copy of our example environment file:

```
cp .env.example .env
```

You should update the .env file to match your organisation. Finally, generate an application key by running:

```
php artisan key:generate
```

The final step is to open your web browser and navigate to yourinstalldomain.com/admin to access the install wizard, which will create the necessary database tables and allow you to create your admin account.

## License

Commune is free and open source software licensed under the [MIT license](https://opensource.org/licenses/MIT). It is built on the [Laravel](https://laravel.com/) framework, which is also licensed under the MIT license.
