<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Hello,</p>
        <p>You have been invited to vote in the <b>{{ $ballot->title }}</b> ballot.</p>
        <p>You can vote using the link below:</p>
        <a href="{{ url('/vote/'.$token) }}" target="_blank">{{ url('/vote/'.$token) }}</a>
        <p>
            @if(!$ballot->start_date->isPast())
                <b>Ballot opens:</b> {{ $ballot->start_date->format('jS F Y, g:i a') }}<br>
            @endif
            <b>Ballot closes:</b> {{ $ballot->end_date->format('jS F Y, g:i a') }}
        </p>
        <p>If you have any problems, email <a href="mailto:{{ env('MAIL_FROM_ADDRESS') }}">{{ env('MAIL_FROM_ADDRESS') }}</a>.</p>
        <p>Best,</p>
        <p>{{ env('MAIL_FROM_NAME') }}</p>
    </body>
</html>
