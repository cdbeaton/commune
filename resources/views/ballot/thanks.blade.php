@extends('ballot.master')

@section('title', $ballot->title)

@section('content')
<div class="py-5 text-center">
    <h2>{{ $ballot->title }}</h2>
    <p class="lead">Thanks for casting your vote.</p>
</div>
@endsection
