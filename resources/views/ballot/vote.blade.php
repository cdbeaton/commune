@extends('ballot.master')

@section('title', $ballot->title)

@section('content')
<div class="py-5 text-center">
    <h2>{{ $ballot->title }}</h2>
    @if($ballot->intro)
    <p class="lead">{!! nl2br($ballot->intro) !!}</p>
    @endif
</div>
<form method="post">
    @csrf
    @foreach($ballot->questions as $question)
        <div class="row">
            <div class="col">
                <h2 class="mb-3">{{ $question->title }}</h2>
                @if($question->intro)
                <p>{!! nl2br($question->intro) !!}</p>
                @endif
                <div class="d-block my-3">
                    @if($question->type=='stv')
                        <div id="stv{{ $question->id }}" class="container">
                            @foreach($question->options as $option)
                            <div class="row sortable" id="{{ $option->id }}">
                                <div class="col-1 text-right">
                                    <span id="pref{{ $option->id }}" data-option="option{{ $option->id }}">#-</span>
                                </div>
                                <div class="col-11">
                                    <div class="custom-control custom-checkbox">
                                        <input id="option{{ $option->id }}" name="option{{ $option->id }}" value="{{ $option->id }}" class="custom-control-input" type="checkbox">
                                        <label class="custom-control-label" for="option{{ $option->id }}"><p><b>{{ $option->title }}</b></p> @if($option->intro) <p>{{ $option->intro}}</p> @endif</label>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <input type="hidden" name="stv{{ $question->id }}_order" id="stv{{ $question->id }}-order" value="">
                    @else
                        @foreach($question->options as $option)
                            <div class="custom-control custom-radio">
                                <input id="{{ $option->id }}" name="question{{ $question->id }}" value="{{ $option->id }}" type="radio" class="custom-control-input" required>
                                <label class="custom-control-label" for="{{ $option->id }}"><p><b>{{ $option->title }}</b></p> @if($option->intro) <p>{{ $option->intro}}</p> @endif</label>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    @endforeach
    <div class="pt-3 pb-5">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Submit Ballot</button>
    </div>
</form>
@endsection

@section('js')
<script>
$(function() {
    @foreach($ballot->questions()->where('type', 'stv')->get() as $question)
        $("#stv{{ $question->id }}").sortable({
            create: function (event, ui) {
                var order = $(this).sortable('toArray');
                $("#stv{{ $question->id }}-order").attr('value', order);
            },
            update: function (event, ui) {
                var order = $(this).sortable('toArray');
                $("#stv{{ $question->id }}-order").attr('value', order);
                update_order('stv{{ $question->id }}');
            }
        });
    @endforeach

    $("form :input").change(function() {
        @foreach($ballot->questions()->where('type', 'stv')->get() as $question)
        update_order('stv{{ $question->id }}');
        @endforeach
    });

    function update_order(stv) {
        var i = 1;
        $('#' + stv).find('span').each(function(){
            var checkbox = $(this).data('option');
            if ($('#' + checkbox).is(':checked')) {
                $(this).html('#' + i);
                i++;
            } else {
                $(this).html('#-');
            }
        });
    }

    $('form :input').bind('click', function(){
        $(this).focus();
    });
});
</script>
@endsection
