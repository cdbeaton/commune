@extends('ballot.master')

@section('title', $ballot->title)

@section('content')
<div class="py-5 text-center">
    <h2>{{ $ballot->title }}</h2>
    <p class="lead">This ballot opens on {{ $ballot->start_date->format('jS F Y') }}.</p>
</div>
@endsection
