@extends('admin.master')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-sm btn-outline-primary float-right" data-toggle="modal" data-target="#createModal">Create</button>
                    Ballots
                </div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Title</th>
                                <th scope="col">Time left</th>
                                <th scope="col">Turnout</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($ballots->count()>0)
                                @foreach($ballots as $b)
                                    <tr>
                                        <th scope="row">{{ $b->id }}</th>
                                        <td><a href="{{ route('admin.ballot.questions', ['ballot' => $b->id]) }}">{{ $b->title }}</a></td>
                                        <td>{!! $b->time_left(true) !!}</td>
                                        <td>{!! $b->turnout(true) !!}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td colspan="4">No ballots yet!</td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create ballot</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="createForm" method="post">
                @csrf
                <div class="modal-body">
                    <div class="alert alert-danger d-none" id="errorsDiv">
                        <ul class="mb-0" id="errorsList">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title" value="{{ old('title') }}">
                    </div>
                    <div class="form-group">
                        <label for="intro">Introduction</label>
                        <textarea class="form-control" id="intro" name="intro" row="3">{{ old('intro') }}</textarea>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <label for="start_date">Start date</label>
                            <input type="date" class="form-control" id="start_date" name="start_date" placeholder="Start date" required value="{{ old('start_date') }}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="start_time">Start time</label>
                            <input type="time" class="form-control" id="start_time" name="start_time" placeholder="start_time" value="{{ old('start_time') }}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="end_date">End date</label>
                            <input type="date" class="form-control" id="end_date" name="end_date" placeholder="End date" required value="{{ old('end_date') }}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="end_time">End time</label>
                            <input type="time" class="form-control" id="end_time" name="end_time" placeholder="End time" value="{{ old('end_time') }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="createSubmit">Create</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function () {
    $('body').on('click', '#createSubmit', function(){
        var createForm = $("#createForm");
        var formData = createForm.serialize();

        $('#errorsList').empty();
        $('#errorsDiv').addClass('d-none');

        $.ajax({
            url:'{{ route('admin.ballot.store') }}',
            type:'POST',
            data:formData,
            success:function(data) {
                if(data.errors) {
                    $('#errorsDiv').removeClass('d-none');
                    $.each(data.errors, function(k, v) {
                        console.log('<li>' + v[0] + '</li>');
                        $('#errorsList').append('<li>' + v[0] + '</li>');
                    });
                }
                if(data.success) {
                    location.reload(true);
                }
            },
        });
    });
});
</script>
@endsection
