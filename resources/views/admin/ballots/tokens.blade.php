@extends('admin.master')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    Send {{ $ballot->title }}
                </div>
                <div class="card-body">
                    <form action="{{ route('admin.token.store', ['ballot' => $ballot->id]) }}" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="emails">Email list</label>
                            <textarea class="form-control" id="emails" name="emails" row="3">{{ old('emails') }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </form>
                    <hr>
                    @if($ballot->tokens->count()==0)
                        <p>No ballots have currently been sent.</p>
                    @else
                        <p>A total of {{ $ballot->tokens->count() }} emails have been sent.</p>
                        <p>Of those, {{ $ballot->tokens()->where('used', 1)->count() }} vote(s) have been submitted.</p>
                        <button type="button" data-toggle="modal" data-target="#deleteTokensModal" class="btn btn-sm btn-outline-danger">Clear all tokens and votes</button>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<div class="modal fade" id="deleteTokensModal" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                Are you <u>definitely</u> sure?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form method="POST" action="{{ route('admin.token.destroy', ['ballot' => $ballot->id]) }}" id="delete-form">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
