@extends('admin.master')

@section('content')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="row justify-content-center">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('admin.token.index', ['ballot' => $ballot->id]) }}" class="btn btn-sm btn-outline-primary float-right">Send emails</a>
                    {{ $ballot->title }}
                </div>
                <div class="card-body">
                    @if($ballot->intro)<p class="text-muted">{!! nl2br($ballot->intro) !!}</p>@endif
                    <div class="mb-3">
                        <button type="button" data-toggle="modal" data-target="#deleteBallotModal" class="btn btn-sm btn-outline-danger mr-1">Delete ballot</button>
                        <button type="button" class="btn btn-sm btn-outline-primary mr-1" data-toggle="modal" data-target="#editBallotModal">Edit ballot</button>
                        <button type="button" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target="#createModal">Add question</button>
                    </div>
                    <div class="accordion" id="accordion">
                        @foreach($ballot->questions as $question)
                            <div class="card">
                                <div class="card-header" id="heading{{ $question->id }}">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse{{ $question->id }}" aria-expanded="true" aria-controls="collapse{{ $question->id }}">
                                            {{ $question->title }} ({{ ($question->type=='stv' ? 'STV' : 'FPTP') }})
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse{{ $question->id }}" class="collapse {{ (request()->session()->get('question')==$question->id ? 'show' : '') }}" aria-labelledby="heading{{ $question->id }}" data-parent="#accordion">
                                    <div class="card-body pb-0">
                                        @if($question->intro)<p class="text-muted">{!! nl2br($question->intro) !!}</p>@endif
                                        <div class="mb-3">
                                            <button type="button" data-toggle="modal" data-target="#deleteQuestionModal" class="btn btn-sm btn-outline-danger mr-1 deleteQuestion" data-question="{{ $question->id }}">Delete question</button>
                                            <button type="button" class="btn btn-sm btn-outline-primary mr-1" id="editQuestion{{ $question->id }}">Edit question</button>
                                            <button type="button" class="btn btn-sm btn-outline-primary mr-1" id="addOption{{ $question->id }}">Add option</button>
                                            @if($question->type=='stv') <button type="button" data-toggle="modal" data-target="#calculateWinnersModal" class="btn btn-sm btn-outline-primary calculateWinners" data-question="{{ $question->id }}" data-max="{{ $question->options()->count() }}">Calculate winners</button> @endif
                                        </div>
                                        @if($question->options->count()>0)
                                            @foreach($question->options as $option)
                                                <p>
                                                    <strong>{{ $option->title }}:</strong> {{ ($question->type=='stv') ? $option->votes()->where('stv_position', 1)->count().' first preferences' : $option->votes->count() }} ({{ $option->percentage() }})<br>
                                                    {{ $option->intro }}
                                                </p>
                                            @endforeach
                                        @else
                                            <p>No options yet.</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="deleteBallotModal" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form method="POST" action="{{ route('admin.ballot.destroy', ['ballot' => $ballot->id]) }}" id="delete-form">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editBallotModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit ballot</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editBallotForm" method="patch">
                @csrf
                <div class="modal-body">
                    <div class="alert alert-danger d-none" id="editBallotErrorsDiv">
                        <ul class="mb-0" id="editBallotErrorsList">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title" value="{{ $ballot->title }}">
                    </div>
                    <div class="form-group">
                        <label for="intro">Introduction</label>
                        <textarea class="form-control" id="intro" name="intro" row="3">{{ $ballot->intro }}</textarea>
                    </div>
                    <div class="form-row">
                        <div class="col-md-3 mb-3">
                            <label for="start_date">Start date</label>
                            <input type="date" class="form-control" id="start_date" name="start_date" placeholder="Start date" required value="{{ date('Y-m-d', strtotime($ballot->start_date)) }}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="start_time">Start time</label>
                            <input type="time" class="form-control" id="start_time" name="start_time" placeholder="start_time" value="{{ date('H:i:s', strtotime($ballot->start_date)) }}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="end_date">End date</label>
                            <input type="date" class="form-control" id="end_date" name="end_date" placeholder="End date" required value="{{ date('Y-m-d', strtotime($ballot->end_date)) }}">
                        </div>
                        <div class="col-md-3 mb-3">
                            <label for="end_time">End time</label>
                            <input type="time" class="form-control" id="end_time" name="end_time" placeholder="End time" value="{{ date('H:i:s', strtotime($ballot->end_date)) }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="editBallotSubmit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="deleteQuestionModal" tabindex="-1" role="dialog" aria-labelledby="delete" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                Are you sure?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <form method="POST" action="{{ route('admin.question.destroy', ['ballot' => $ballot->id]) }}" id="delete-form">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <input type="hidden" id="deleteQuestionID" name="question" value="">
                    <input type="submit" class="btn btn-danger" value="Delete">
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editQuestionModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit question</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="editQuestionForm" method="patch">
                @csrf
                <input type="hidden" name="question" id="editQuestionID" value="">
                <div class="modal-body">
                    <div class="alert alert-danger d-none" id="editQuestionErrorsDiv">
                        <ul class="mb-0" id="editQuestionErrorsList">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="editQuestionTitle">Title</label>
                        <input type="text" class="form-control" id="editQuestionTitle" name="title" placeholder="Enter title">
                    </div>
                    <div class="form-group">
                        <label for="editQuestionIntro">Introduction (optional)</label>
                        <textarea class="form-control" id="editQuestionIntro" name="intro" row="3"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="editQuestionType">Type</label>
                        <select class="form-control" name="type" id="editQuestionType">
                            <option>First-past-the-post</option>
                            <option value="stv">Single transferable vote</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="editQuestionSubmit">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add question</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="createForm" method="post">
                @csrf
                <div class="modal-body">
                    <div class="alert alert-danger d-none" id="errorsDiv">
                        <ul class="mb-0" id="errorsList">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title">
                    </div>
                    <div class="form-group">
                        <label for="intro">Introduction (optional)</label>
                        <textarea class="form-control" id="intro" name="intro" row="3">{{ old('intro') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="type">Type</label>
                        <select class="form-control" name="type" id="type">
                            <option>First-past-the-post</option>
                            <option value="stv">Single transferable vote</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="createSubmit">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="addOptionModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add option</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="addForm" method="post">
                @csrf
                <input type="hidden" name="question" id="question" value="">
                <div class="modal-body">
                    <div class="alert alert-danger d-none" id="addErrorsDiv">
                        <ul class="mb-0" id="addErrorsList">
                        </ul>
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter title">
                    </div>
                    <div class="form-group">
                        <label for="intro">Description</label>
                        <input type="text" class="form-control" id="intro" name="intro" placeholder="Enter description (optional)">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="addSubmit">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="calculateWinnersModal" tabindex="-1" role="dialog" aria-labelledby="calculate" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <form id="calculateForm" method="post">
                @csrf
                <input type="hidden" id="calculateWinnersID" name="question" value="">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="numberOfWinners">How many winners do you need?</label>
                        <select class="form-control" name="winners" id="numberOfWinners">
                        </select>
                    </div>
                    <p id="winners"></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="calculateSubmit">Calculate</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
$(document).ready(function () {
    $('body').on('click', '#editBallotSubmit', function(){
        var editBallotForm = $("#editBallotForm");
        var formData = editBallotForm.serialize();

        $('#editBallotErrorsList').empty();
        $('#editBallotErrorsDiv').addClass('d-none');

        $.ajax({
            url:'{{ route('admin.ballot.update', ['ballot' => $ballot->id]) }}',
            type:'PATCH',
            data:formData,
            success:function(data) {
                if(data.errors) {
                    $('#editBallotErrorsDiv').removeClass('d-none');
                    $.each(data.errors, function(k, v) {
                        console.log('<li>' + v[0] + '</li>');
                        $('#editBallotErrorsList').append('<li>' + v[0] + '</li>');
                    });
                }
                if(data.success) {
                    location.reload(true);
                }
            },
        });
    });

    $('body').on('click', '#editQuestionSubmit', function(){
        var editQuestionForm = $("#editQuestionForm");
        var formData = editQuestionForm.serialize();

        $('#editQuestionErrorsList').empty();
        $('#editQuestionErrorsDiv').addClass('d-none');

        $.ajax({
            url:'{{ route('admin.question.update', ['ballot' => $ballot->id]) }}',
            type:'PATCH',
            data:formData,
            success:function(data) {
                if(data.errors) {
                    $('#editQuestionErrorsDiv').removeClass('d-none');
                    $.each(data.errors, function(k, v) {
                        console.log('<li>' + v[0] + '</li>');
                        $('#editQuestionErrorsList').append('<li>' + v[0] + '</li>');
                    });
                }
                if(data.success) {
                    location.reload(true);
                }
            },
        });
    });

    $('body').on('click', '#createSubmit', function(){
        var createForm = $("#createForm");
        var formData = createForm.serialize();

        $('#errorsList').empty();
        $('#errorsDiv').addClass('d-none');

        $.ajax({
            url:'{{ route('admin.question.store', ['ballot' => $ballot->id]) }}',
            type:'POST',
            data:formData,
            success:function(data) {
                if(data.errors) {
                    $('#errorsDiv').removeClass('d-none');
                    $.each(data.errors, function(k, v) {
                        console.log('<li>' + v[0] + '</li>');
                        $('#errorsList').append('<li>' + v[0] + '</li>');
                    });
                }
                if(data.success) {
                    location.reload(true);
                }
            },
        });
    });

    $('body').on('click', '#addSubmit', function(){
        var createForm = $("#addForm");
        var formData = createForm.serialize();

        $('#addErrorsList').empty();
        $('#addErrorsDiv').addClass('d-none');

        $.ajax({
            url:'{{ route('admin.option.store', ['ballot' => $ballot->id]) }}',
            type:'POST',
            data:formData,
            success:function(data) {
                if(data.errors) {
                    $('#addErrorsDiv').removeClass('d-none');
                    $.each(data.errors, function(k, v) {
                        console.log('<li>' + v[0] + '</li>');
                        $('#addErrorsList').append('<li>' + v[0] + '</li>');
                    });
                }
                if(data.success) {
                    location.reload(true);
                }
            },
        });
    });

    @foreach($ballot->questions as $question)
    $('#editQuestion{{ $question->id }}').click(function() {
        var editQuestionTitle = {!! json_encode(['value' => $question->title]) !!};
        var editQuestionIntro = {!! json_encode(['value' => $question->intro]) !!};
        var editQuestionType = {!! json_encode(['value' => $question->type]) !!};
        $('#editQuestionID').val({{ $question->id }});
        $('#editQuestionTitle').val(editQuestionTitle.value);
        $('#editQuestionIntro').val(editQuestionIntro.value);
        $('#editQuestionType').val(editQuestionType.value);
        $('#editQuestionModal').modal('show');
    });
    $('#addOption{{ $question->id }}').click(function() {
        $('#question').val({{ $question->id }});
        $('#addOptionModal').modal('show');
    });
    @endforeach

    $(".deleteQuestion").click(function() {
        var deleteQuestionID = $(this).data('question');
        $("#deleteQuestionID").val(deleteQuestionID);
    });

    $(".calculateWinners").click(function() {
        var calculateWinnersID = $(this).data('question');
        var max = $(this).data('max');
        var text = '';
        $("#calculateWinnersID").val(calculateWinnersID);
        $('#winners').empty();
        $("#numberOfWinners").empty();
        for (i = 1; i <= max; i++) {
            $("#numberOfWinners").append('<option value="' + i + '">' + i + '</option>');
        }
    });

    $('body').on('click', '#calculateSubmit', function(){
        $('#winners').empty();
        var calculateForm = $("#calculateForm");
        var formData = calculateForm.serialize();

        $.ajax({
            url:'{{ route('admin.question.calculate', ['ballot' => $ballot->id]) }}',
            type:'POST',
            data:formData,
            success:function(data) {
                if(data.outcomes) {
                    $.each(data.outcomes, function(k, v) {
                        $('#winners').append(v + '<br />');
                    });
                }
            },
        });
    });
});
</script>
@endsection
