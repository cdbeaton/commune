@extends('ballot.master')

@section('title', 'Install Commune')

@section('content')
<div class="py-5 text-center">
    <h2>Install Commune</h2>
    <p class="lead">Welcome to Commune! I haven't written any documentation yet, so you'll have to
    figure this part out for yourself. Good luck!</p>
</div>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post">
    @csrf
    <div class="mb-3">
        <label for="name">Your name</label>
        <input type="text" class="form-control" id="name" name="name" placeholder="" value="{{ old('name') }}" required>
        <div class="invalid-feedback">
            Please enter a valid name.
        </div>
    </div>
    <div class="mb-3">
        <label for="email">Your email</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="you@example.scot" value="{{ old('email') }}" required>
        <div class="invalid-feedback">
            Please enter a valid email address.
        </div>
    </div>
    <div class="mb-3">
        <label for="password">Your password</label>
        <input type="password" class="form-control" id="password" name="password" required>
        <div class="invalid-feedback">
            Please enter a valid password.
        </div>
    </div>
    <div class="pt-3 pb-5">
        <button class="btn btn-primary btn-lg btn-block" type="submit">Install</button>
    </div>
</form>
@endsection

@section('js')
<script>
(function () {
    'use strict'

    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation')

        // Loop over them and prevent submission
        Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
        })
    }, false)
})()
</script>
@endsection
