<?php

namespace App\Exceptions;

use Exception;

class WinConditionException extends Exception
{
    /**
     * Log exception event with this logging callback
     */
    public function report()
    {
        // TODO: decide if exception condition requires custom logging
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        // TODO: develop correct server response view for this exception
        // return response(...);
    }
}
