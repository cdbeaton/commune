<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Ballot;

class BallotOpen extends Mailable
{
    use Queueable, SerializesModels;

    public $ballot;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Ballot $ballot, $token)
    {
        $this->ballot = $ballot;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->ballot->title)
            ->view('emails.ballot');
    }
}
