<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * Get the ballot.
     */
    public function ballot()
    {
        return $this->belongsTo('App\Models\Ballot');
    }

    /**
     * Get the options for the question.
     */
    public function options()
    {
        return $this->hasMany('App\Models\Option');
    }
}
