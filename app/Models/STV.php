<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Question;
use App\Models\Vote;
use App\Exceptions\WinConditionException;

class STV extends Model
{
    use HasFactory;

    public function __construct($question_id, $total_winners, array $attributes = array())
    {
        // Create empty collections
        $this->outcomes = collect();
        $this->winners = collect();
        $this->losers = collect();

        // Initialise attributes from database
        $this->total_winners = $total_winners;
        $this->question = Question::find($question_id);
        $this->option_ids = $this->question->options()->pluck('id')->toArray();
        $this->total_votes = Vote::whereIn('option_id', $this->option_ids)->get()->unique('stv_token')->count();
        $this->total_candidates = $this->question->options()->count();

        // Initialise attributes from methods
        $this->quota = $this->droop_quota();
        $this->bundles = $this->bundle_votes();
        $this->first_prefs = $this->sort_votes();

        parent::__construct($attributes);
    }

    public function add_outcome($outcome)
    {
        $outcomes = $this->outcomes;
        $outcomes[] = $outcome;
        $this->outcomes = $outcomes;
    }

    public function calculate_winners()
    {
        if ($this->question->options->count() < $this->total_winners) {
            throw new WinConditionException("There are fewer candidates than possible winners");
        }

        // Show the quota
        $this->add_outcome('Quota: ' . $this->quota);

        for ($i = 1; $this->winners->count() < $this->total_winners; $i++) {
            $this->add_outcome('Round ' . $i);

            // Get vote bundles sorted by vote totals for each candidate
            $this->first_prefs = $this->sort_votes();
            $candidates_left = $this->total_candidates - $this->winners->count() - $this->losers->count();
            $remaining_seats = $this->total_winners - $this->winners->count();

            // Dump current tallies for each surviving candidate
            foreach ($this->first_prefs as $key => $value) {
                $o = Option::find($key);
                $this->add_outcome($o->title . ' - ' . $value);
            }

            // If there are as many candidates remaining as seats available, elect them and break
            if ($candidates_left <= $remaining_seats) {
                foreach ($this->first_prefs as $key => $value) {
                    $o = Option::find($key);
                    $this->add_outcome('ELECTED: ' . $o->title);
                    $this->winners->push($key);
                }
                break;
            }

            // If any candidates meet the quota, they should win and their votes should transfer
            if (count($this->meets_quota())>0) {
                foreach (array_keys($this->meets_quota()) as $e) {
                    // Add them to the winners
                    $o = Option::find($e);
                    $this->add_outcome('ELECTED: ' . $o->title);
                    $this->winners->push($e);

                    // Calculate the transfer value
                    $surplus = round(($this->first_prefs[$e] - $this->quota) / $this->first_prefs[$e], 4);
                    $this->add_outcome('SURPLUS: ' . $surplus . ' ('.($this->first_prefs[$e] - $this->quota).')');

                    // Transfer votes from elected candidate
                    $eliminated_options = array_merge([], $this->winners->toArray());
                    $eliminated_options = array_merge($eliminated_options, $this->losers->toArray());
                    $new_bundles = collect();
                    foreach ($this->bundles as $b) {
                        if ($b->preferences->first() == $e) {
                            $b->value = round($b->value * $surplus, 4);
                            while (in_array($b->preferences->first(), $eliminated_options)) {
                                $b->preferences->shift();
                            }
                            if ($b->preferences->count() > 0) {
                                $t = Option::find($b->preferences->first());
                                $this->add_outcome('Transferring bundle worth '.$b->value.' to ' . $t->title);
                            } else {
                                $this->add_outcome('One bundle ran out of transfers');
                            }
                        }
                        if ($b->preferences->count() > 0) {
                            $new_bundles->push($b);
                        }
                    }
                    $this->bundles = $new_bundles;

                    // Sort votes again post-redistribution
                    $this->first_prefs = $this->sort_votes();
                }

                // If we have filled all the seats we want, break
                if ($this->winners->count() >= $this->total_winners) {
                    break;
                }
            } else { // If there are no winners, we do eliminations
                $candidates_left = $this->total_candidates - $this->winners->count() - $this->losers->count();
                $remaining_seats = $this->total_winners - $this->winners->count();
                $max_allowed_eliminations = $candidates_left - $remaining_seats;

                // Identify the candidate(s) in last place
                $last_place = $this->first_prefs[array_key_last($this->first_prefs)];

                $eliminations = array_filter(
                    $this->first_prefs,
                    function ($value) use ($last_place) {
                        return ($value <= $last_place);
                    }
                );

                $eliminated_options = array_keys($eliminations);
                $all_eliminated = array_merge($eliminated_options, $this->winners->toArray());
                $all_eliminated = array_merge($all_eliminated, $this->losers->toArray());

                // If we have too many eliminations, coin toss to pick survivor(s) for the next round
                if (count($eliminated_options) > $max_allowed_eliminations) {
                    $reduction = count($eliminated_options) - $max_allowed_eliminations;
                    shuffle($eliminated_options);
                    while (count($eliminated_options) > $max_allowed_eliminations) {
                        $survivor = array_shift($eliminated_options);
                        $o = Option::find($survivor);
                        $this->add_outcome('SURVIVED COIN TOSS: ' . $o->title);
                    }
                }

                foreach ($eliminated_options as $e) {
                    $o = Option::find($e);
                    $this->add_outcome('ELIMINATED: ' . $o->title);
                    $this->losers->push($e);

                    // Redistribute votes for eliminated candidates
                    $new_bundles = collect();
                    foreach ($this->bundles as $b) {
                        if ($b->preferences->first() == $e) {
                            while (in_array($b->preferences->first(), $all_eliminated)) {
                                $b->preferences->shift();
                            }
                            if ($b->preferences->count() > 0) {
                                $t = Option::find($b->preferences->first());
                                $this->add_outcome('Transferring bundle worth '.$b->value.' to ' . $t->title);
                            } else {
                                $this->add_outcome('One bundle ran out of transfers');
                            }
                        }
                        if ($b->preferences->count() > 0) {
                            $new_bundles->push($b);
                        }
                    }
                    $this->bundles = $new_bundles;
                }
            }
        }

        // TODO: Return results in a table of rounds
        return $this->outcomes;
    }

    public function bundle_votes()
    {
        $all_bundles = collect();
        $options = $this->question->options()->pluck('id');
        $stv_tokens = Vote::whereIn('option_id', $options)->get()->unique('stv_token')->pluck('stv_token');

        foreach ($stv_tokens as $t) {
            $bundle = new \stdClass();
            $bundle->value = 1;
            $bundle->preferences = collect();
            $votes = Vote::where('stv_token', $t)->orderBy('stv_position')->get();
            foreach ($votes as $v) {
                $bundle->preferences->push($v->option_id);
            }
            $all_bundles->push($bundle);
        }

        return $all_bundles;
    }

    public function sort_votes()
    {
        // Tally up the votes, taking into account the transfer value
        $tallies = [];
        foreach ($this->bundles as $b) {
            $top_pref = $b->preferences->first();
            if (array_key_exists($top_pref, $tallies)) {
                $tallies[$top_pref] = $tallies[$top_pref] + $b->value;
            } else {
                $tallies[$top_pref] = $b->value;
            }
        }

        // Include options with 0 tallies if they have not yet been eliminated or transferred
        $missing = array_diff($this->option_ids, array_merge($this->winners->toArray(), $this->losers->toArray(), array_keys($tallies)));
        foreach ($missing as $m) {
            $tallies[$m] = 0;
        }

        // Return options in order from most tallies to least
        arsort($tallies);
        return $tallies;
    }

    /**
     * Returns the Droop Quota (min. votes required to be elected) of a given ballot count
     *
     * @return int
     */
    public function droop_quota()
    {
        return (int) ($this->total_votes / ($this->total_winners + 1)) + 1;
    }

    /**
     * Returns an array of the options which have met quota
     */
    public function meets_quota()
    {
        return array_filter(
            $this->first_prefs,
            function ($value, $key) {
                if ($value >= $this->quota) {
                    return true;
                }
            },
            ARRAY_FILTER_USE_BOTH
        );
    }
}
