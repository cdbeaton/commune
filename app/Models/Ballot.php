<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Ballot extends Model
{
    use HasFactory;

    public $dates = ['start_date', 'end_date'];

    /**
     * Get the questions for the ballot.
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question');
    }

    /**
     * Get the tokens for the ballot.
     */
    public function tokens()
    {
        return $this->hasMany('App\Models\Token');
    }

    public function time_left($styled=false)
    {
        if ($this->end_date->isPast()) {
            $string = 'Closed '.$this->end_date->diffForHumans(Carbon::now(), Carbon::DIFF_RELATIVE_TO_NOW);
            $class = 'text-danger';
        }

        if (!$this->end_date->isPast()) {
            $string = 'Closes in '.$this->end_date->diffForHumans(Carbon::now(), Carbon::DIFF_ABSOLUTE);
            $class = 'text-success';
        }

        if (!$this->start_date->isPast()) {
            $string = 'Starts in '.$this->start_date->diffForHumans(Carbon::now(), Carbon::DIFF_ABSOLUTE);
        }

        if ($styled) {
            return '<span class="'.$class.'">'.$string.'</span>';
        }

        return $string;
    }

    public function turnout($styled=false)
    {
        $total_tokens = $this->tokens->count();

        if ($total_tokens == 0) {
            $value = 'N/A';
        } else {
            $used_tokens = $this->tokens()->where('used', 1)->count();
            $value = round(($used_tokens/$total_tokens)*100);

            if ($styled) {
                $class = 'text-danger';
                if ($value > 40) { $class = 'text-warning'; }
                if ($value > 60) { $class = 'text-success'; }
                return '<span class="'.$class.'">'.$value.'%</span>';
            }
        }

        return $value;
    }
}
