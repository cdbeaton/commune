<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Option extends Model
{
    use HasFactory;

    public $timestamps = false;

    /**
     * Get the question.
     */
    public function question()
    {
        return $this->belongsTo('App\Models\Question');
    }

    /**
     * Get the votes for the option.
     */
    public function votes()
    {
        return $this->hasMany('App\Models\Vote');
    }

    /**
     * Get the percentage votes for the option.
     */
    public function percentage()
    {
        if ($this->question->type=='stv') {
            $option_votes = $this->votes()->where('stv_position', 1)->count();
            $total_votes = $this->question->options()->withCount(['votes' => function (Builder $query) {
                $query->where('stv_position', 1);
            }, ])->get()->sum('votes_count');
        } else {
            $option_votes = $this->votes()->count();
            $total_votes = $this->question->options()->withCount('votes')->get()->sum('votes_count');
        }
        if($option_votes==0) { return '0%'; }
        $percentage = ($option_votes / $total_votes)*100;
        return round($percentage).'%';
    }
}
