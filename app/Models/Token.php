<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Ballot;
use App\Mail\BallotOpen;
use Illuminate\Support\Facades\Mail;

class Token extends Model
{
    use HasFactory;

    protected $fillable = ['used'];

    public $timestamps = false;

    protected static function booted()
    {
        static::creating(function ($token) {
            $token->token = uniqid();
            $token->used = false;
        });
    }

    /**
     * Get the ballot.
     */
    public function ballot()
    {
        return $this->belongsTo('App\Models\Ballot');
    }

    public function sendEmail($email)
    {
        Mail::to($email)->send(new BallotOpen($this->ballot, $this->token));
    }
}
