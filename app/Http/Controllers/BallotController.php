<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Token;
use App\Models\Ballot;
use App\Models\Vote;

class BallotController extends Controller
{
    public function show(Request $request, $token)
    {
        $checkToken = Token::where('token', $token);

        if ($checkToken->count()>0) {
            $token = $checkToken->get()->first();
            $ballot = Ballot::where('id', $token->ballot_id)->with('questions.options')->first();

            if (!$ballot) { abort(404); }

            if ($token->used!=true) {
                if (!$ballot->start_date->isPast()) { return view('ballot.pending', ['ballot' => $ballot]); }
                if ($ballot->end_date->isPast()) { return view('ballot.closed', ['ballot' => $ballot]); }
                return view('ballot.vote', ['ballot' => $ballot]);
            }

            return view('ballot.thanks', ['ballot' => $ballot]);
        }

        abort(404);
    }

    public function post(Request $request, $token)
    {
        $checkToken = Token::where('token', $token);

        if ($checkToken->count()>0) {
            $token = $checkToken->get()->first();
            $ballot = Ballot::where('id', $token->ballot_id)->with('questions.options')->first();

            if ($token->used!=true) {
                // Save votes for any STV questions
                foreach($ballot->questions()->where('type', 'stv')->get() as $question) {
                    $stv_token = uniqid();
                    $stv_position = 0;
                    $order = $request->get('stv'.$question->id.'_order');
                    foreach(explode(',', $order) as $option) {
                        // Only store a preference if the box was checked
                        if ($request->get('option'.$option)) {
                            $stv_position++;
                            $vote = new Vote;
                            $vote->question_id = $question->id;
                            $vote->option_id = $option;
                            $vote->stv_position = $stv_position;
                            $vote->stv_token = $stv_token;
                            $vote->save();
                        }
                    }
                }
                // Save votes for any FPTP questions
                foreach($ballot->questions()->whereNull('type')->get() as $question) {
                    $choice = $request->get('question'.$question->id);
                    if ($choice) {
                        $vote = new Vote;
                        $vote->question_id = $question->id;
                        $vote->option_id = $choice;
                        $vote->save();
                    }
                }
                $token->update(['used' => true]);
            }

            return redirect(url('/vote/'.$token->token));
        }

        abort(404);
    }

    /**
     * Show the create ballot form.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.ballots.create');
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        if (!$request->get('start_time')) { $start_time = '00:00:00'; } else { $start_time = $request->get('start_time'); }
        if (!$request->get('end_time')) { $end_time = '00:00:00'; } else { $end_time = $request->get('end_time'); }

        $ballot = new Ballot;
        $ballot->title = $request->get('title');
        $ballot->intro = $request->get('intro');
        $ballot->start_date = $request->get('start_date').' '.$start_time;
        $ballot->end_date = $request->get('end_date').' '.$end_time;
        $ballot->save();

        return response()->json(['success' => '1']);
    }

    public function update(Request $request, Ballot $ballot)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after_or_equal:start_date'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        if (!$request->get('start_time')) { $start_time = '00:00:00'; } else { $start_time = $request->get('start_time'); }
        if (!$request->get('end_time')) { $end_time = '00:00:00'; } else { $end_time = $request->get('end_time'); }

        $ballot->title = $request->get('title');
        $ballot->intro = $request->get('intro');
        $ballot->start_date = $request->get('start_date').' '.$start_time;
        $ballot->end_date = $request->get('end_date').' '.$end_time;
        $ballot->save();

        return response()->json(['success' => '1']);
    }

    public function destroy(Request $request, Ballot $ballot)
    {
        $ballot->questions()->delete();
        $ballot->tokens()->delete();
        $ballot->delete();

        return redirect(route('admin'));
    }

    /**
     * Show the ballot questions.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function questions(Ballot $ballot)
    {
        return view('admin.ballots.questions', compact('ballot'));
    }
}
