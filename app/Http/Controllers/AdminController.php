<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ballot;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $ballots = Ballot::orderBy('end_date', 'desc')->paginate(15);
        return view('admin.ballots.index', compact('ballots'));
    }
}
