<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;

class InstallerController extends Controller
{
    public function show() {
        return view('admin.install');
    }

    public function post(Request $request) {
        // Validate the admin account details
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);

        if ($validator->fails()) {
            return redirect()
                ->route('install')
                ->withErrors($validator)
                ->withInput();
        }

        // Create the necessary tables
        Artisan::call('migrate', ['--force' => true]);

        // Create the admin account
        $user = new User;
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->save();

        // Create a file to indicate that Commune is installed
        $fp = fopen(base_path() . '/installed', 'w');

        // Redirect to the admin dashboard
        return redirect()->route('admin');
    }
}
