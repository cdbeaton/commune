<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Ballot;
use App\Models\Token;
use App\Models\Vote;

class TokenController extends Controller
{
    /**
     * Show the ballot tokens.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Ballot $ballot)
    {
        return view('admin.ballots.tokens', compact('ballot'));
    }

    /**
     * Send ballot emails.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request, Ballot $ballot)
    {
        $emails = preg_split('/\r\n|\r|\n/', $request->get('emails'));

        foreach ($emails as $email) {
            $token = new Token;
            $token->ballot_id = $ballot->id;
            $token->save();
            $token->sendEmail($email);
        }

        return redirect(route('admin.token.index', ['ballot' => $ballot->id]));
    }

    public function destroy(Request $request, Ballot $ballot)
    {
        // Destroy all tokens
        $tokens = $ballot->tokens;
        foreach ($tokens as $t) { $t->delete(); }
        // Destroy all votes
        $questions = $ballot->questions->pluck('id');
        $votes = Vote::whereIn('question_id', $questions)->get();
        foreach ($votes as $v) { $v->delete(); }

        return redirect(route('admin.token.index', ['ballot' => $ballot->id]));
    }
}
