<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Option;
use App\Models\Question;
use App\Models\Ballot;
use Illuminate\Support\Facades\Validator;

class OptionController extends Controller
{
    public function store(Request $request, Ballot $ballot)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'question' => 'exists:questions,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $option = new Option;
        $option->title = $request->get('title');
        $option->intro = $request->get('intro');
        $option->question_id = $request->get('question');
        $option->save();

        $request->session()->flash('question', $option->question_id);
        return response()->json(['success' => '1']);
    }
}
