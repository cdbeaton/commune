<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Ballot;
use App\Models\STV;
use Illuminate\Support\Facades\Validator;

class QuestionController extends Controller
{
    public function store(Request $request, Ballot $ballot)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $question = new Question;
        $question->title = $request->get('title');
        $question->intro = $request->get('intro');
        $question->ballot_id = $ballot->id;
        if ($request->get('type')=='stv') { $question->type = 'stv'; }
        $question->save();

        return response()->json(['success' => '1']);
    }

    public function update(Request $request, Ballot $ballot)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'question' => 'exists:questions,id'
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $question = Question::find($request->get('question'));
        $question->title = $request->get('title');
        $question->intro = $request->get('intro');
        $question->ballot_id = $ballot->id;
        if ($request->get('type')=='stv') { $question->type = 'stv'; }
        $question->save();

        $request->session()->flash('question', $question->id);
        return response()->json(['success' => '1']);
    }

    public function destroy(Request $request, Ballot $ballot)
    {
        $question = Question::find($request->get('question'));
        if ($question) { $question->delete(); }
        return redirect(route('admin.ballot.questions', ['ballot' => $ballot->id]));
    }

    public function calculate(Request $request, Ballot $ballot)
    {
        $question = Question::find($request->get('question'));
        if ($question) {
            $stv = new STV($question->id, $request->get('winners'));
            $winners = $stv->calculate_winners();
            return response()->json(['outcomes' => $winners]);
        }
        // TODO: Proper error catching
        return false;
    }
}
