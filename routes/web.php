<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InstallerController;
use App\Http\Controllers\BallotController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\TokenController;
use App\Http\Controllers\OptionController;

Route::group(['middleware' => ['notinstalled']], function () {
    Route::get('/install', [InstallerController::class, 'show'])->name('install');
    Route::post('/install', [InstallerController::class, 'post'])->name('install.post');
});

Route::get('/vote/{token}', [BallotController::class, 'show']);
Route::post('/vote/{token}', [BallotController::class, 'post']);

Route::group(['middleware' => ['installed']], function () {
    Auth::routes(['register' => false]);

    Route::group(['middleware' => ['auth']], function () {
        Route::get('/admin', [AdminController::class, 'index'])->name('admin');
        Route::get('/admin/ballots/create', [BallotController::class, 'create'])->name('admin.ballot.create');
        Route::post('/admin/ballots', [BallotController::class, 'store'])->name('admin.ballot.store');
        Route::patch('/admin/ballots/{ballot}', [BallotController::class, 'update'])->name('admin.ballot.update');
        Route::delete('/admin/ballots/{ballot}', [BallotController::class, 'destroy'])->name('admin.ballot.destroy');
        Route::get('/admin/ballots/{ballot}', [BallotController::class, 'questions'])->name('admin.ballot.questions');
        Route::get('/admin/ballots/{ballot}/details', [BallotController::class, 'edit'])->name('admin.ballot.edit');
        Route::patch('/admin/ballots/{ballot}/details', [BallotController::class, 'update'])->name('admin.ballot.update');
        Route::delete('/admin/ballots/{ballot}', [BallotController::class, 'destroy'])->name('admin.ballot.destroy');
        Route::post('/admin/ballots/{ballot}/questions', [QuestionController::class, 'store'])->name('admin.question.store');
        Route::delete('/admin/ballots/{ballot}/questions', [QuestionController::class, 'destroy'])->name('admin.question.destroy');
        Route::patch('/admin/ballots/{ballot}/questions', [QuestionController::class, 'update'])->name('admin.question.update');
        Route::post('/admin/ballots/{ballot}/calculate', [QuestionController::class, 'calculate'])->name('admin.question.calculate');
        Route::get('/admin/ballots/{ballot}/tokens', [TokenController::class, 'index'])->name('admin.token.index');
        Route::post('/admin/ballots/{ballot}/tokens', [TokenController::class, 'store'])->name('admin.token.store');
        Route::delete('/admin/ballots/{ballot}/tokens', [TokenController::class, 'destroy'])->name('admin.token.destroy');
        Route::post('/admin/ballots/{ballot}/options', [OptionController::class, 'store'])->name('admin.option.store');
    });
});
